const express = require("express");
const checkOutRoutes = express.Router();
const auth = require("../authorization");
const checkOutController = require("../controllers/checkOutController");

// CheckOut
checkOutRoutes.post(
    "/checkoutItems",
    auth.verifyToken,
    checkOutController.checkOutSelectedItems
);
// Checkout One
checkOutRoutes.post(
    "/checkOutOne/:productId",
    auth.verifyToken,
    checkOutController.checkSpecificItems
);

module.exports = checkOutRoutes;
